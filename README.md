# Syndic sync

A web scraping synchronisation of documents that compares the list of personal documents on webportaal3000 with the previous document scrape.

Whenever there are changes, a mail will be sent to myself with a summary of which documents are added (or deleted).

## Getting started

This is a very specific script that you probably have no use for. However, you could use it as a starting point to automate other stuff on legacy websites or webapplications that don't have a public API.

For this specific script you'd need to have an account at webportaal3000 and the following environment variables are used in the GitLab pipeline that runs every night:

```
PORTAL_LOGIN
PORTAL_PASS
GMAIL_USER
GMAIL_PASS
```
