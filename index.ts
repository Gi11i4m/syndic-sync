import filterConsole from 'filter-console'
import { PlaywrightFluent } from 'playwright-fluent'
import { authenticate, reportChanges, saveDocumentsList } from './stories'
import { getWebDriver } from './util/webdriver'

require('dotenv').config()
filterConsole(['Execution context was destroyed'])
;(async () => {
  let p: PlaywrightFluent | undefined
  try {
    p = getWebDriver()
    await p
      .runStory(authenticate, {
        login: process.env.PORTAL_LOGIN!,
        pass: process.env.PORTAL_PASS!
      })
      .runStory(saveDocumentsList)
      .runStory(reportChanges)
  } catch (e) {
    console.error(e)
    process.exit(1)
  } finally {
    p && (await p.close())
  }
})()
