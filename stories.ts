import { existsSync, readFileSync, writeFileSync } from 'fs'
import { Story, StoryWithProps } from 'playwright-fluent'
import { getDiffFor } from './util/diff'
import { log } from './util/logger'
import { sendMail } from './util/mailer'

const DOC_TABLE_SELECTOR = `//div[@id='A4_']`
const PREV_DOCS_NAME = 'previous-documents.txt'
const DOCS_NAME = 'documents.txt'

export const authenticate: StoryWithProps<{
  login: string
  pass: string
}> = async (p, { login, pass }) =>
  await p
    .navigateTo(
      'https://opensyndic.3xc.be/NL/PAGE_syndic_login.awp?P1=c3luY3Jvc3M'
    )
    .waitForStabilityOf(() => p.isVisible('#M62'))
    .click('#M62')
    .runStory(async () => {
      log('💻 Logging in')
    })
    .click('#A8')
    .typeText(login)
    .click('#A3')
    .typeText(pass)
    .click('#A4')

export const saveDocumentsList: Story = async p =>
  await p
    .click('#A3')
    .click('#A6')
    .runStory(async () => {
      log('📃 Generating documents list')
    })
    .click('#A28')
    .waitUntil(() =>
      p.selector(`${DOC_TABLE_SELECTOR}//a[text()='2018']`).isVisible()
    )
    .runStory(async () => {
      const documents = await p.getInnerTextOf(DOC_TABLE_SELECTOR)
      log(`💾 Saving ${DOCS_NAME}`)
      writeFileSync(DOCS_NAME, documents)
    })

export const reportChanges: Story = async p => {
  await p

  log(`🔎 Looking for changes compared to ${PREV_DOCS_NAME}`)

  const changes = getDiffFor(
    existsSync(PREV_DOCS_NAME) ? readFileSync(PREV_DOCS_NAME).toString() : '',
    readFileSync(DOCS_NAME).toString()
  )

  changes
    ? log(`📊 Changes: \n${changes}`) && (await sendMail(changes))
    : log('🚫 No changes')
}
