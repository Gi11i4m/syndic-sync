import * as diff from 'diff'

export const getDiffFor = (origin: string, target: string) =>
  diff
    .diffTrimmedLines(origin, target)
    .filter(({ added, removed }) => added || removed)
    .map(({ added, value }) =>
      added ? `Toegevoegd: ${value}` : `Verwijderd: ${value}`
    )
    .join('')
    .trim()
