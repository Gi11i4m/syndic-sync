export const log = (data: any): true => {
  console.log('\x1b[36m%s\x1b[0m', data)
  return true
}
