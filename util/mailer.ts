import { createTransport } from 'nodemailer'
import { log } from './logger'

export const sendMail = async (text: string) =>
  createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    secure: true,
    auth: {
      user: process.env.GMAIL_USER,
      pass: process.env.GMAIL_PASS
    }
  })
    .sendMail({
      from: '"Gilliam (GitLab)" <gi11i4m@gmail.com>',
      to: 'gi11i4m@gmail.com',
      subject: '🔺 A new document has been uploaded to webportaal3000',
      text
    })
    .then(() => log('📧 Mail with changes sent'))
    .catch(error => log(`💥 Failed to send mail: ${error}`) && process.exit(1))
