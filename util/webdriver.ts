import { PlaywrightFluent } from 'playwright-fluent'
import { log } from './logger'

export const getWebDriver = () =>
  log('🏗  Creating webdriver') &&
  new PlaywrightFluent().withBrowser('chrome').withOptions({ headless: true })
